class: center, middle

# [Plan] group conversation

2019-03-19

Please add questions to the [group conversation Google doc agenda](https://docs.google.com/document/d/1zELoftrommhRdnEOAocE6jea6w7KngUDjTUQBlMCNAU)

[plan]: https://about.gitlab.com/direction/plan/

---

# [Team updates][team] 👥

- 👋 2019-02-11
  - [Donald Cook][donald] joined as Frontend Engineering Manager  🇺🇸
  - [Alexandru Croitor][alexandru] joined as Backend Engineer 🇲🇩
- 👋 2019-03-04
  - [Alexis Ginsberg][alexis] joined as Senior UX Designer 🇺🇸
  - [Walmyr Lima e Silva Filho][walmyr] joined as Senior Test Automation Engineer 🇧🇷🇳🇱
- 👋 2019-03-25
  - C.A. joining as Senior Backend Engineer 🇨🇦🇳🇿
- 👋 2019-05-06 / 2019-05-07
  - M.H. joining as Frontend Engineer 🇨🇦🇸🇰
  - E.G. joining as Backend Engineer 🇺🇾🇬🇧

[team]: https://about.gitlab.com/handbook/engineering/dev-backend/plan/#plan-team

---

# Shipped changes 🚢

- [Start discussion from any comment] - 🙌 [Annabel], [Constance],
  [Heinrich], and [Winnie]
- [Reorder child epics] - 🙌 [Heinrich] and [Rajat]
- [Better indicator for moved issues] - 🙌 [Andrew] and [Annabel]
- [Task list improvements] - 🙌 [Brett] and [Fatih]
- [Roadmap scrolling] - 🙌 [Annabel] and [Kushal]
  [Annabel]
- [Enable commenting in 'activity only'] - 🙌 [Annabel] and
  [Kushal]
- [Show web header / footer in emails] - 🙌 [Alexandru] and [Annabel]
- [Ancestor line in epic sidebar] - 🙌 [Annabel], [Jan], and [Rajat]
- [Show recently-visited boards] - 🙌 [Annabel], [Heinrich], and [Rajat]

[Task list improvements]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/23938
[Roadmap scrolling]: https://gitlab.com/gitlab-org/gitlab-ee/issues/7325
[Reorder child epics]: https://gitlab.com/gitlab-org/gitlab-ee/issues/7328
[Better indicator for moved issues]: https://gitlab.com/gitlab-org/gitlab-ce/issues/36445
[Enable commenting in 'activity only']: https://gitlab.com/gitlab-org/gitlab-ce/issues/51819
[Show web header / footer in emails]: https://gitlab.com/gitlab-org/gitlab-ce/issues/56863
[Ancestor line in epic sidebar]: https://gitlab.com/gitlab-org/gitlab-ee/issues/8845
[Show recently-visited boards]: https://gitlab.com/gitlab-org/gitlab-ee/issues/7714
[Start discussion from any comment]: https://gitlab.com/gitlab-org/gitlab-ce/issues/30299

[Andrew]: https://gitlab.com/afontaine

---

# From the community 💖

- [Filter for confidential issues] - 🙌 [Robert]
- [Youtrack integration] - 🙌 [Kotau]
- [Promote milestones from API] - 🙌 [Nermin]
- [API support for group labels] - 🙌 [Robert]
- [Expose text color from labels API] - 🙌 [Robert]
- [Compact links in Jira] - 🙌 [Elan]
- WIP: [Open Project integration] - 🙌 [Xin]

[API support for group labels]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/21368
[Filter for confidential issues]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/24960
[Compact links in Jira]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/25609
[Youtrack integration]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/25361
[Promote milestones from API]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/25203
[Expose text color from labels API]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/25172
[Open Project integration]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/24888

[Robert]: https://gitlab.com/razer6
[Elan]: https://gitlab.com/glensc
[Nermin]: https://gitlab.com/nermin.vehabovic90
[Kotau]: https://gitlab.com/bessorion
[Xin]: https://gitlab.com/mengxin

---

# In progress

## Workflows 🔁

- [First-class workflows] -> [Key-value replacement labels]
- See the [comment explaining this] and the video:

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/fo2-5VaDSww" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[First-class workflows]: https://gitlab.com/gitlab-org/gitlab-ee/issues/2059
[Key-value replacement labels]: https://gitlab.com/gitlab-org/gitlab-ee/issues/9175
[comment explaining this]: https://gitlab.com/gitlab-org/gitlab-ee/issues/2059#note_148371186

---

# In progress

## [Jira integration] 🛠

- Replaces DVCS connector integration
- For Jira Cloud + GitLab.com only in first iteration
- Jira Cloud + GitLab self-hosted coming later
- Jira Server + any GitLab depends on Atlassian
- Integrating with Jira will now be a one-click install
- Support for [creating branches and MRs] from Jira can be added in the future

[Jira integration]: https://gitlab.com/gitlab-org/gitlab-ee/issues/9641
[creating branches and MRs]: https://gitlab.com/gitlab-org/gitlab-ee/issues/2650

---

# Security work 🔒

.w60[![](security-issues.png)]

- More security work since we [made our HackerOne program public]
- Good news: [most of them are from older releases]. Could be because
  newer issues haven't been found yet, but we don't have evidence that
  we are often introducing new vulnerabilities
- (We verified with [Kathy] that this is OK to share)

[made our HackerOne program public]: https://about.gitlab.com/2018/12/12/gitlab-hackerone-bug-bounty-program-is-public-today/
[most of them are from older releases]: https://gitlab.com/gl-retrospectives/plan/issues/19#note_145557222
[Kathy]: https://gitlab.com/kathyw

---

# Recent blog posts ✍

- [Remote work enables innovation] - 🙌 [Victor]
- [Automating retrospectives] - 🙌 [Sean]
- WIP: [Fixing task lists] - 🙌 [Brett], [Fatih], and [Victor]
- WIP: [Migrating Markdown processing to CommonMark] - 🙌 [Brett]
- WIP: [Elasticsearch lessons learned] - 🙌 [Mario]
- WIP: [Optimizing remote work flexibility] - 🙌 [Jarka]

[Remote work enables innovation]: https://about.gitlab.com/2019/02/27/remote-enables-innovation/
[Automating retrospectives]: https://about.gitlab.com/2019/03/07/how-we-used-gitlab-to-automate-our-monthly-retrospectives/
[Fixing task lists]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19114
[Migrating Markdown processing to CommonMark]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19048
[Elasticsearch lessons learned]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/19302
[Optimizing remote work flexibility]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15431

---

# Recent videos 📺

- [Latest Plan weekly meeting]
- [New GitLabber coffee chat - Quality and test automation, using excitement as leverage]
- [Plan stage - Goals and strategy]
- [Custom workflows and value stream management]

[New GitLabber coffee chat - Quality and test automation, using excitement as leverage]: https://www.youtube.com/watch?v=z1TE0OhKuVU
[Plan stage - Goals and strategy]: https://www.youtube.com/watch?v=K2wlXStHXAo
[Latest Plan weekly meeting]: https://www.youtube.com/watch?v=GZ1QSe5K4aQ
[Custom workflows and value stream management]: https://www.youtube.com/watch?v=fo2-5VaDSww

[felipe]: https://gitlab.com/felipe_artur
[jarka]: https://gitlab.com/jarka
[brett]: https://gitlab.com/digitalmoksha
[jan]: https://gitlab.com/jprovaznik
[mario]: https://gitlab.com/mdelaossa
[heinrich]: https://gitlab.com/engwan
[patrick]: https://gitlab.com/pderichs
[alexandru]: https://gitlab.com/acroitor
[sean]: https://gitlab.com/smcgivern
[pedro]: https://gitlab.com/pedroms
[annabel]: https://gitlab.com/annabeldunstone
[alexis]: https://gitlab.com/uhlexsis
[kushal]: https://gitlab.com/kushalpandya
[constance]: https://gitlab.com/okoghenun
[fatih]: https://gitlab.com/fatihacet
[andré]: https://gitlab.com/andr3
[winnie]: https://gitlab.com/winh
[rajat]: https://gitlab.com/rajatgitlab
[donald]: https://gitlab.com/donaldcook
[ramya]: https://gitlab.com/at.ramya
[walmyr]: https://gitlab.com/wlsf82
[victor]: https://gitlab.com/victorwu
