class: center, middle

# Plan update

31 July 2018

[Sean McGivern](https://gitlab.com/smcgivern)

Engineering Manager, [Plan]

[Plan]: https://about.gitlab.com/handbook/engineering/dev-backend/plan/

---

# Discussion ➡ Plan

* [Oswaldo] and [Mark] are [moving to Create] as of 11.3, to help them make
  merge requests and repository management even better!

* The rest is now Plan, mapping to our [categories]

    * [Jan], Senior Developer
    * [Felipe], Developer
    * [Jarka], Developer
    * [Mario], Developer
    * [Chantal], Junior Developer
    * Plus [André], [Fatih], [Kushal], and [Constance] on frontend
    * [Pedro] and [Annabel] on UX
    * And [Victor] as PM

[moving to Create]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13098
[categories]: https://about.gitlab.com/handbook/product/categories/#dev

---

# Hiring

* Q3 OKR:

    > Plan: Source 25 candidates by July 15 and hire 1 developer: 50 sourced
    > (100%), hired X (X%)

* [Standardise the technical interview]
* Our (GitLab-only) [hiring documentation] is pretty comprehensive now
* We sourced more because the Discussion target was 50 sourced and two hires

[Standardise the technical interview]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13302
[hiring documentation]: https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Backend

---

# Performance improvements

* Q3 OKR:

    > Plan: Implement 10 [performance improvements]: X/10 (X%)

* Three in 11.2, three in 11.3 (this is the summit month, so we have lower
  capacity), and four in 11.4
* Mostly cover Plan, some that are more in Create but we will finish as we
  picked the specific issues before the team changes

[performance improvements]: https://gitlab.com/groups/gitlab-org/-/epics/247

---

# 11.1 🚢

* [Filter by filename in code search] - thanks [Mario]!

* [Subgroups in JIRA Development Panel] - thanks [Mark]!

* [CommonMark as the default Markdown renderer] - thanks [Brett] and
  [blackst0ne]!

* [Support direct upload to object storage via Workhorse for attachments] -
  thanks [Jan]!

* [Todos for epics]
    * I merged this before it had a database review (human error)
    * This lead to a [P3 production incident]
    * [Danger] will post a comment when an MR needs DB review
    * Thanks [Jarka], [Yorick], and [Rémy]!

[Filter by filename in code search]: https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/5590
[Subgroups in JIRA Development Panel]: https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/6290
[CommonMark as the default Markdown renderer]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/19331
[Support direct upload to object storage via Workhorse for attachments]: https://gitlab.com/gitlab-org/gitlab-ce/issues/44663
[Todos for epics]: https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/6142
[P3 production incident]: https://gitlab.com/gitlab-com/production/issues/7
[Danger]: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/19066

---

# 11.2 🚆

* Todos for epics - [take two]!

* [Milestone lists in boards] - thanks [Oswaldo] and [Constance]!

* [Integrate milestone dates with epic dates] - thanks [Mark]!

* [Show total weight of issues in board columns] - thanks [Felipe] and [Simon]!

* [Board assignee lists API] - thanks [Chantal]!

[take two]: https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/6542
[Milestone lists in boards]: https://gitlab.com/gitlab-org/gitlab-ee/issues/6469
[Integrate milestone dates with epic dates]: https://gitlab.com/gitlab-org/gitlab-ee/issues/6470
[Show total weight of issues in board columns]: https://gitlab.com/gitlab-org/gitlab-ee/issues/3772
[Batch comments in merge requests]: https://gitlab.com/gitlab-org/gitlab-ee/issues/1984

[Felipe]: https://gitlab.com/felipe_artur
[Jarka]: https://gitlab.com/jarka
[Oswaldo]: https://gitlab.com/oswaldo
[Jan]: https://gitlab.com/jprovaznik
[Mario]: https://gitlab.com/mdelaossa
[Chantal]: https://gitlab.com/crollison
[Mark]: https://gitlab.com/lulalala

[Pedro]: https://gitlab.com/pedroms
[Annabel]: https://gitlab.com/annabeldunstone
[Kushal]: https://gitlab.com/kushalpandya
[Constance]: https://gitlab.com/okoghenun
[Fatih]: https://gitlab.com/fatihacet
[André]: https://gitlab.com/andr3
[Victor]: https://gitlab.com/victorwu

[Brett]: https://gitlab.com/digitalmoksha
[blackst0ne]: https://gitlab.com/blackst0ne
[Yorick]: https://gitlab.com/yorickpeterse
[Rémy]: https://gitlab.com/rymai
[Simon]: https://gitlab.com/psimyn
