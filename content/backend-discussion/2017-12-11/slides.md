# Discussion update

* Date: 11 December 2017
* Who: [Sean McGivern](https://gitlab.com/smcgivern)
  * [Discussion backend team](https://about.gitlab.com/handbook/backend/#discussion)
  lead

---

# [Better commit comments](https://gitlab.com/gitlab-org/gitlab-ce/issues/31847)

.w90[![](commit-comment-mr.png)]

* Thanks [Micael] and [Douwe]!

---

# [Create MR from email](https://gitlab.com/gitlab-org/gitlab-ce/issues/32878)

.w90[![](create-mr-from-email.gif)]

* Thanks [Jan]!

---

# [Epics](https://gitlab.com/gitlab-org/gitlab-ee/issues/3254)

.w90[![](epics.png)]

* Thanks [Jarka]!

---

# Backstage 🎭

* Specs for GitLab QA:
    * [Protected branches](https://gitlab.com/gitlab-org/gitlab-qa/issues/77)
    * [Squash and rebase](https://gitlab.com/gitlab-org/gitlab-qa/issues/78)
    * Thanks [Felipe] and [Micael]!
* Chipping away at performance issues
  ([AP1](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Discussion&label_name[]=AP1) / [AP2](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Discussion&label_name[]=AP2))
    * Most of the low-hanging fruit has been taken
    * Thanks [Oswaldo] for chipping away!

---

# Coming up 📆

* [Move uploads to object storage](https://gitlab.com/gitlab-org/gitlab-ee/issues/4163)
    * Needed for GCP migration
* Continue with epics
    * [Reorder issues](https://gitlab.com/gitlab-org/gitlab-ee/issues/3694)
    * [API support](https://gitlab.com/gitlab-org/gitlab-ee/issues/4225)
* Continue with AP issues
* Make [GitLab.com blog builds faster](https://gitlab.com/gitlab-com/www-gitlab-com/issues/1265)
* [Bring rebase to CE](https://gitlab.com/gitlab-org/gitlab-ce/issues/40301)

[Felipe]: https://gitlab.com/felipe_artur
[Jarka]: https://gitlab.com/jarka
[Oswaldo]: https://gitlab.com/oswaldo
[Micael]: https://gitlab.com/mbergeron
[Jan]: https://gitlab.com/jprovaznik

[Douwe]: https://gitlab.com/DouweM
