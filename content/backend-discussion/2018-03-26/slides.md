# Discussion update

* Date: 26 March 2018
* Who: [Sean McGivern](https://gitlab.com/smcgivern)
  * [Discussion backend](https://about.gitlab.com/handbook/backend/#discussion)
  engineering manager

---

.right[
[![](felipe.jpg)](felipe.jpg)
]

# [Felipe]

* [JIRA](https://docs.gitlab.com/ee/user/project/integrations/jira.html) [expert]
* [Project feature visibility](https://docs.gitlab.com/ee/user/permissions.html#project-features) [expert]
* [Issue boards](https://docs.gitlab.com/ee/user/project/issue_board.html) [expert]
* [Image discussions](https://docs.gitlab.com/ee/user/discussions/index.html#image-discussions) [expert]

---

.right[
[![](jarka.jpg)](jarka.jpg)
]

# [Jarka]

* [JIRA](https://docs.gitlab.com/ee/user/project/integrations/jira.html) [expert]
* [Snippets](https://docs.gitlab.com/ee/user/snippets.html) [expert]
* [Portfolio management](https://docs.gitlab.com/ee/user/group/epics/index.html) [expert]
* Currently on parental leave, looking after Eliška!

---

.right[
[![](oswaldo.jpg)](oswaldo.jpg)
]

# [Oswaldo]

* Merge request [coach](https://about.gitlab.com/roles/merge-request-coach/)
* [GitLab API](https://docs.gitlab.com/ee/api/) [expert]
* [Related issues](https://docs.gitlab.com/ee/user/project/issues/related_issues.html) [expert]
* [Issue boards](https://docs.gitlab.com/ee/user/project/issue_board.html) [expert]
* [Diffs expert in training](https://gitlab.com/gitlab-org/gitlab-ce/issues/43614)

---

.right[
[![](jan.jpg)](jan.jpg)
]

# [Jan]

* [Portfolio management](https://docs.gitlab.com/ee/user/group/epics/index.html) [expert]
* Unofficial non-Elasticsearch search expert!

---

.right[
[![](mario.jpg)](mario.jpg)
]

# [Mario]

* [Elasticsearch expert in training](https://gitlab.com/gitlab-org/gitlab-ee/issues/5032)

---

# But what did we do lately?

* [One group board in Libre](https://gitlab.com/gitlab-org/gitlab-ce/issues/38337)
    * Thanks [Felipe]!
* [Approvers API](https://gitlab.com/gitlab-org/gitlab-ee/issues/183)
    * Thanks [Mario]!
* [Discussions API (excludes diff discussions)](https://gitlab.com/gitlab-org/gitlab-ce/issues/41860)
    * Thanks [Jan]!
* Merge request performance improvements:
  [1](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/17630) /
  [2](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/17659)
    * Fixes some Gitaly N+1s
    * Thanks [Oswaldo]!
* [Object storage in Libre](https://gitlab.com/gitlab-org/gitlab-ce/issues/40781)
    * 10.7
    * Thanks [Micael], [Kamil], [Shinya], et al!

---

# And more!

* Portfolio management
    * [Epics API](https://gitlab.com/gitlab-org/gitlab-ee/issues/4732)
    * [Add/remove labels on epics](https://gitlab.com/gitlab-org/gitlab-ee/issues/3727)
    * [Filter epics by label](https://gitlab.com/gitlab-org/gitlab-ee/issues/4032)
* [Kubernetes deploy button](https://gitlab.com/gitlab-org/gitlab-ce/issues/42044)

---

# Coming up

* Still working on [batch commenting in MRs](https://gitlab.com/groups/gitlab-org/-/epics/23)
* [Comment threads in epics](https://gitlab.com/gitlab-org/gitlab-ee/issues/3889)
* [Support subgroups for group labels](https://gitlab.com/gitlab-org/gitlab-ce/issues/40915)
* More in 10.8, we are still planning it

---

# [One more thing](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/17742)

.left[
![](performance-bar.gif)

Thanks [Filipa]!
]

[Felipe]: https://gitlab.com/felipe_artur
[Jarka]: https://gitlab.com/jarka
[Oswaldo]: https://gitlab.com/oswaldo
[Micael]: https://gitlab.com/mbergeron
[Jan]: https://gitlab.com/jprovaznik
[Mario]: https://gitlab.com/mdelaossa

[expert]: https://about.gitlab.com/roles/expert/

[Kamil]: https://gitlab.com/ayufan
[Shinya]: https://gitlab.com/dosuken123
[Filipa]: https://gitlab.com/filipa
