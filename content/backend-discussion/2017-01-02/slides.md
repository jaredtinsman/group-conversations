# Discussion update

* Date: 2 January 2017
* Who: [Sean McGivern](https://gitlab.com/smcgivern)
  * [Discussion backend team](https://about.gitlab.com/handbook/backend/#discussion)
  lead

---

# Last release: 8.15 👍

* [Auto deploy](https://docs.gitlab.com/ce/ci/autodeploy/index.html)
  * Added a button to set up auto deploy for a repo, just like adding a README
  * Thanks [Adam](https://gitlab.com/adamniedzielski)!
* [Bitbucket importer](https://docs.gitlab.com/ce/workflow/importing/import_projects_from_bitbucket.html)
  * Added pull requests with comments
  * Added milestones
  * Added wiki
  * Thanks [Valery](https://gitlab.com/vsizov)!

---

# Last release: 8.15 👎

* [Unapprove merge request](https://gitlab.com/gitlab-org/gitlab-ee/issues/761) (EE) slipped
  * We'll get it in 8.16, just email templates left
* We made the
  [system note design changes](https://gitlab.com/gitlab-org/gitlab-ce/issues/24784)
  (adding icons, CI statuses to commits) slip while we figure out the best way
  to implement this

---

# Last release: 8.15 🚀

* Some amazing community contributions:
  * [Mathematics support in Markdown](https://docs.gitlab.com/ce/user/markdown.html#math)
      * <code>$&#96;a^2 + b^2 = c^2&#96;$</code> to try it!
  * [Cleaner merge commit messages](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/7722)
      * No more checklist items, screenshots, etc.
  * [Shorthand cross-project references](https://docs.gitlab.com/ce/user/markdown.html#special-gitlab-references)
      * `gitlab-org/gitlab-ce#1` becomes `gitlab-ce#1` in the same namespace

---

# Next release: 8.16 ✨

* [Squash](https://gitlab.com/gitlab-org/gitlab-ee/issues/150) and
  [rebase](https://gitlab.com/gitlab-org/gitlab-ee/issues/895) (both EE)
    * Going to get rid of the manual rebase button, and just rebase if needed
      when accepting an MR
    * Will allow squashing with any type of merge strategy (merge commit, merge
      commit with semi-linear history, fast-forward)

---

# Next release: 8.16 ✨

* [GitLab license finder](https://gitlab.com/gitlab-org/gitlab-ee/issues/1125) (EE)
  * First-class support for the
    [`LicenseFinder`](https://github.com/pivotal/LicenseFinder) gem
  * We already have a Rake task for this in the Rails project - this is to
    integrate into GitLab itself

---

# Next release: 8.16 ✨

* Other bug fixes and stretch goals, because ...
* There's a [GitLab Summit](https://gitlab.com/summits/Summit-2017) in Mexico! 🇲🇽

---

# Future goals 🗓

* Unblock the frontend for
  [system note changes](https://gitlab.com/gitlab-org/gitlab-ce/issues/24784)
* Make our
  [custom notification settings](https://gitlab.com/gitlab-org/gitlab-ce/issues/24892)
  both clearer and more granular
* All development teams:
  [improve the release process](https://gitlab.com/gitlab-com/organization/issues/1)
  (this issue is currently marked as confidential)
