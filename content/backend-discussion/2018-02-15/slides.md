# Discussion update

* Date: 15 February 2018
* Who: [Sean McGivern](https://gitlab.com/smcgivern)
  * [Discussion backend](https://about.gitlab.com/handbook/backend/#discussion)
  engineering manager

---

# [Epic roadmap view](https://gitlab.com/gitlab-org/gitlab-ee/issues/3559)

.w90[![](epic-roadmap-view.png)]

* Thanks [Jan] and [Kushal]!

---

# [Link to MR from commit page](https://gitlab.com/gitlab-org/gitlab-ce/issues/2383)

.w90[![](show-mr-on-commit-page.png)]

* Thanks [Hiroyuki]!

---

# [Uploads to object storage](https://gitlab.com/gitlab-org/gitlab-ee/issues/4163)

* We had a few issues after deploying, but we're getting there
    * [Potential data loss](https://gitlab.com/gitlab-org/gitlab-ee/issues/4928)
    * [Option set in gitlab.rb didn't work](https://gitlab.com/gitlab-org/gitlab-ee/issues/4915)
    * [fog-google was using HTTP, not HTTPS](https://gitlab.com/gitlab-org/gitlab-ee/issues/4879)
    * [Post-mortem](https://gitlab.com/gitlab-com/infrastructure/issues/3693)
* Thanks [Micael]!

---

# Performance 🚤

* [Issues / MRs dashboard](https://gitlab.com/gitlab-org/gitlab-ce/issues/37143)
  * Can't show the improvement on a
  [chart](https://performance.gitlab.net/dashboard/db/rails-controllers?orgId=1&var-action=DashboardController%23issues) 😞
  * The remaining timeouts are only when
  [no filters are used](https://gitlab.com/gitlab-org/gitlab-ce/issues/43246)
  * Thanks [Felipe]!
* [Search](https://gitlab.com/gitlab-org/gitlab-ce/issues/40540)
  * Global search (mostly) doesn't time out any more
      * Even with the lower statement timeout of 15 seconds
  * We still need to address a
  [few more cases](https://gitlab.com/gitlab-org/gitlab-ce/issues/43242)
  * Thanks [Jan]!

---

# Performance 🚀

* [MR widget polling is faster](https://gitlab.com/gitlab-org/gitlab-ce/issues/36876)
  * Thanks [Oswaldo]!
* [Profiling requests from the Rails console is easier](https://docs.gitlab.com/ee/development/profiling.html#profiling-a-url)
  * Profile the current version of the app
  * Make the proposed change in the console
  * Profile again
  * Repeat until fast!

---

# Elsewhere

* [Show why a notification email was sent](https://gitlab.com/gitlab-org/gitlab-ce/issues/41532)
  * In future, we will
  [add more](https://gitlab.com/gitlab-org/gitlab-ce/issues/42062)!
  * Thanks [Mario]! And welcome!
* [Search API](https://gitlab.com/gitlab-org/gitlab-ee/issues/2245)
  * Thanks [Jarka]!
* [Show issues and MRs from subgroups on group page](https://gitlab.com/gitlab-org/gitlab-ce/issues/30106)
  * Thanks [Jarka]!

---

# [Batch commenting in MRs](https://gitlab.com/groups/gitlab-org/-/epics/23)

* Currently blocked by some refactors to make this possible
  * [Move MR comments tab to Vue](https://gitlab.com/gitlab-org/gitlab-ce/issues/38178)
  * [Move MR changes tab to Vue](https://gitlab.com/gitlab-org/gitlab-ce/issues/42882)
* Bumped to 10.7
* Thanks [Felipe], [Fatih], and [Simon]!

---

# Coming up 📆

* More [portfolio management](https://gitlab.com/groups/gitlab-org/-/epics/48)
    * [Comment thread on epics](https://gitlab.com/gitlab-org/gitlab-ee/issues/3889)
    * [API for epic and issue comment threads](https://gitlab.com/gitlab-org/gitlab-ce/issues/41860)
* Help with [GKE integration](https://gitlab.com/gitlab-org/gitlab-ce/issues/35956)
* [Approvers API](https://gitlab.com/gitlab-org/gitlab-ee/issues/183)
* [Improve MR diffs load times](https://gitlab.com/gitlab-org/gitlab-ce/issues/38408)

[Felipe]: https://gitlab.com/felipe_artur
[Jarka]: https://gitlab.com/jarka
[Oswaldo]: https://gitlab.com/oswaldo
[Micael]: https://gitlab.com/mbergeron
[Jan]: https://gitlab.com/jprovaznik
[Mario]: https://gitlab.com/mariodelaossa

[Kushal]: https://gitlab.com/kushalpandya
[Fatih]: https://gitlab.com/fatihacet
[Simon]: https://gitlab.com/psimyn

[Hiroyuki]: https://gitlab.com/hiroponz
