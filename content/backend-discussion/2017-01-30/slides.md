# Discussion update

* Date: 30 January 2017
* Who: [Sean McGivern](https://gitlab.com/smcgivern)
  * [Discussion backend team](https://about.gitlab.com/handbook/backend/#discussion)
  lead

---

# Last release: 8.16 👍

* There was a [summit](https://gitlab.com/summits/Summit-2017)! 🇲🇽
* [Jarka](https://gitlab.com/jarka) and [Oswaldo](https://gitlab.com/oswaldo) started
* [Unapprove merge request](https://gitlab.com/gitlab-org/gitlab-ee/issues/761) (EE)
  * Thanks [Jeanine](https://gitlab.com/jneen)!
* [`/merge` slash command](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/7746)
  * Thanks [Jarka](https://gitlab.com/jarka)!

---

# Last release: 8.16 👎

* The release itself was painful. We're going to have a much
  [stricter feature freeze](https://gitlab.com/gitlab-org/release-tools/merge_requests/76)
  in future
  * Thanks [Dmitriy](https://gitlab.com/dzaporozhets)!
* [Squash](https://gitlab.com/gitlab-org/gitlab-ee/issues/150) slipped, but will
  have a better implementation as a result of the extra review time!
* [License finder](https://gitlab.com/gitlab-org/gitlab-ee/issues/1125) (EE)
  also slipped due to illness

---

# Next releases: 8.17 and 9.0 ✨

* We're adding an [API v4](https://gitlab.com/gitlab-org/gitlab-ce/issues/20070)
  and deprecating v3
* We're still working on squash and license finder
  * License finder is tricky, as it is not designed to run on untrusted code
* Help [get Elasticsearch in shape](https://gitlab.com/gitlab-org/gitlab-ce/issues/27084)
* Get slow, frequently-used pages to be less slow (but still frequently-used):
  [1](https://gitlab.com/gitlab-org/gitlab-ce/issues/24944)
  [2](https://gitlab.com/gitlab-org/gitlab-ce/issues/25503)
  [3](https://gitlab.com/gitlab-org/gitlab-ce/issues/27164)
  [4](https://gitlab.com/gitlab-org/gitlab-ce/issues/27165)
  [5](https://gitlab.com/gitlab-org/gitlab-ce/issues/27166)
  [6](https://gitlab.com/gitlab-org/gitlab-ce/issues/27168)

---

# Future goals 🗓

* Move the existing notes polling to use the
  [ETags proposal](https://gitlab.com/gitlab-org/gitlab-ce/issues/26926)
  * Thanks [Andrew](https://gitlab.com/andrewn)!
* The first two from [last time](../2017-01-02/) still apply: system notes and
  custom notification settings
