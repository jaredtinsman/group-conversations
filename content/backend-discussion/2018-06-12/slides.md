# Discussion update

* Date: 12 June 2018
* Who: [Sean McGivern](https://gitlab.com/smcgivern)
  * [Discussion backend](https://about.gitlab.com/handbook/backend/#discussion)
    engineering manager

---

# [Assignee boards] 🚀

.w60[![](assignee-lists.gif)]

* Thanks [Mario], [Kushal], and [Chris]!

---

# Diffs ➕➖

.w48[![](merge_base.png)] .w48[![](raw_diffs.png)]

* Also fixed one [find_commit N+1], [found another]
* Next up: [create a diff statistics endpoint] and [remove non-latest diff files
  from database]
* Thanks [Oswaldo]!

---

# 11.0 🛳

* [The Master role is now called Maintainer]
* [Merge request conflict notifications]
  * Thanks [Mark] for both of those!
* [More flexible issue weights]
  * Thanks [Oswaldo] and [Dennis]!
* [Always on approvals]
  * Thanks [Chantal] and [Lukas]!
* [Many more]
  * Thanks ... everyone!

---

# OKRs

## Deliver 100% of committed issues per release

* 10.8: 9/10 deliverables, 6/6 stretch
* 11.0: 12/14 deliverables, 7/11 stretch
  * Scheduling around frontend refactors is hard
      * I first mentioned [batch commenting] on [17 January]
      * The [frontend refactor] blocking this and [another issue] was merged on
        11 June
  * In general, hard to schedule across teams
      * [Eric], Tim, and [Sarrah's] proposed changes fix this

---

# OKRs

## Make GitLab a [Rails 5 app by default]

* We've fixed a bunch of compatibility issues
  * Thanks [blackst0ne], [Jasper Maes], [Jan], and [Jarka]!
* There are still many issues left, which we plan to fix in 11.1
* After the 11.1 freeze, we will make this the default in master 🤞
* Merge other big changes (like [CommonMark]) after the freeze
  * Or we might [not have a freeze]

---

# OKRs

## Source 150 candidates and hire 3 developers

* Sourced 160 (100%)
* Hired 2 (66%)
  * Unlikely to hit the hiring target
  * We also switched to a [pooled hiring process]
      * This is a good thing - it's a [global optimisation]

[Felipe]: https://gitlab.com/felipe_artur
[Jarka]: https://gitlab.com/jarka
[Oswaldo]: https://gitlab.com/oswaldo
[Jan]: https://gitlab.com/jprovaznik
[Mario]: https://gitlab.com/mdelaossa
[Chantal]: https://gitlab.com/crollison
[Mark]: https://gitlab.com/lulalala

[Kushal]: https://gitlab.com/kushalpandya
[Chris]: https://gitlab.com/cperessini
[Dennis]: https://gitlab.com/dennis
[Lukas]: https://gitlab.com/leipert
[blackst0ne]: https://gitlab.com/blackst0ne
[Jasper Maes]: https://gitlab.com/jlemaes

[17 January]: http://gitlab-org.gitlab.io/group-conversations/backend-discussion/2018-01-17/#9
[batch commenting]: https://gitlab.com/gitlab-org/gitlab-ee/issues/1984
[frontend refactor]: https://gitlab.com/gitlab-org/release/tasks/issues/255
[pooled hiring process]: https://gitlab.com/gitlab-com/people-ops/recruiting/issues/49
[global optimisation]: https://about.gitlab.com/handbook/values/#results
[another issue]: https://gitlab.com/gitlab-org/gitlab-ce/issues/26723
[Rails 5 app by default]: https://gitlab.com/groups/gitlab-org/-/epics/213
[not have a freeze]: https://gitlab.com/gitlab-org/release/tasks/issues/255
[CommonMark]: https://gitlab.com/gitlab-org/gitlab-ce/issues/43011
[Eric]: https://docs.google.com/presentation/d/1Sh7HKIU3uGN9M9X9wry6SCiL3upimFDoQzZPVu6D0gY/edit#slide=id.g3c3874233f_0_33
[Sarrah's]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/12144/diffs
[Assignee boards]: https://gitlab.com/gitlab-org/gitlab-ee/issues/5784
[Create a diff statistics endpoint]: https://gitlab.com/gitlab-org/gitaly/issues/1147
[Remove non-latest diff files from database]: https://gitlab.com/gitlab-org/gitlab-ce/issues/37639
[find_commit N+1]: https://gitlab.com/gitlab-org/gitlab-ce/issues/45190
[found another]: https://gitlab.com/gitlab-org/gitlab-ce/issues/47613
[The Master role is now called Maintainer]: https://gitlab.com/gitlab-org/gitlab-ce/issues/42751
[Merge request conflict notifications]: https://gitlab.com/gitlab-org/gitlab-ce/issues/22041
[Always on approvals]: https://gitlab.com/gitlab-org/gitlab-ee/issues/2452
[More flexible issue weights]: https://gitlab.com/gitlab-org/gitlab-ee/issues/2452
[Many more]: https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=Discussion&milestone_title=11.0&label_name[]=backend
