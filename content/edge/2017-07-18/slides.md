# Edge update

* Date: 18 July 2017
* Who:
  * [Rémy Coutable](https://gitlab.com/rymai): Edge team lead
* Agenda:
  1. Achievements
  1. Q3 OKRs
  1. Questions?

---

# Achievements 0/5

.right[
[![](https://about.gitlab.com/images/team/jen-shin.jpg)](https://about.gitlab.com/images/team/jen-shin.jpg)
]

.left[
* [Jen-Shin] joined the team!
  * He will help crush our Q3 OKRs!
  * Welcome Jen-Shin!
]

---

# Achievements 1/5

* [Faster feature tests](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/12369)!
    * Bypass log-in procedure (i.e. visit `/users/sign_in`, fill & submit the form).
    [![](faster-feature-tests.png)](faster-feature-tests.png)
    * Thanks [Robert]!

* [27 test-related merge requests](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Edge&milestone_title=9.4&label_name[]=test)

---

# Achievements 2/5

* [Replaced some Spinach tests with RSpec](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Edge&label_name[]=backstage&label_name[]=technical%20debt&milestone_title=9.4&search=spinach).
    * Thanks [blackst0ne] and [Alexander Randa]!

* [Reduce parallelization of Spinach jobs (from 10 to 5) and increase the RSpec ones (from 20 to 25)](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/12760).

* Enabled a [few](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/11911) [more](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/11965) [RuboCop](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/12562) [cops](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/12869).

---

# Achievements 3/5

* [93 merged Community Merge Requests in 9.4](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?label_name%5B%5D=Community+Contribution&milestone_title=9.4&sort=created_desc&state=merged).

<iframe width="600" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/1sdppNaGYh1TkhOFdDkSz20YVhc9z9YKtusW-0pmBOtQ/pubchart?oid=16253277&amp;format=interactive"></iframe>

---

# Achievements 4/5

* [How does GitLab QA works?](https://gitlab.com/gitlab-org/gitlab-qa#how-does-it-work)

* [Allow GitLab QA to take a Docker image full address](https://gitlab.com/gitlab-org/gitlab-qa/merge_requests/69).
  * This was part of a cross-team work to be able to [run QA against any CE/EE merge
request](https://gitlab.com/gitlab-org/gitlab-qa/issues/32).
  * Thanks [Balasankar] & [Grzegorz]!

---

# Achievements 5/5

.right[
[![](build-package-job.png)](build-package-job.png)

- - -

[![](omnibus-qa-job.png)](omnibus-qa-job.png)
]

.left[
How to run QA for your merge request:

1. Play the [`build-package` job](https://gitlab.com/gitlab-org/gitlab-ce/-/jobs/22948473)
1. Wait for the [`Trigger:qa` job in Omnibus to run](https://gitlab.com/gitlab-org/omnibus-gitlab/pipelines/9975149).

Notes:

* This actually works for any commit.
* The [Build team] plans to [speed up the triggered package jobs](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/2322).
]

---

# Q3 OKRs 1/3

## Productivity

* [CE Pipelines run in 30 minutes](https://gitlab.com/gitlab-org/gitlab-ce/issues/24899).
  * Currently 50 minutes.
* [Flaky tests don't break `master`](https://gitlab.com/gitlab-org/gitlab-ce/issues/32308).
  * Detect and retry flaky tests in a smart way.
* [CE is automatically merged to EE daily](https://gitlab.com/gitlab-org/gitlab-ce/issues/25870).
  * Avoid conflicts beforehand, and use scheduled pipelines to reduce human labor.
* [GDK based on Kubernetes](https://gitlab.com/gitlab-org/gitlab-development-kit/issues/243) (e.g. minikube).
  * Abstract away GDK installation & setup steps.

---

# Q3 OKRs 2/3

## Quality

* Make [GitLab QA](https://gitlab.com/gitlab-org/gitlab-qa) test [backup/restore](https://gitlab.com/gitlab-org/gitlab-qa/issues/22), [LDAP](https://gitlab.com/gitlab-org/gitlab-qa/issues/3), [Container Registry](https://gitlab.com/gitlab-org/gitlab-qa/issues/49), and [Mattermost](https://gitlab.com/gitlab-org/gitlab-qa/issues/26).
  * Ensure critical components always work.
* [Triage policies](https://about.gitlab.com/handbook/engineering/issues/issue-triage-policies/) are [automatically enforced](https://gitlab.com/gitlab-issue-triage/issue-triage).
  * Keep our issue tracker sane and under control.

---

# Q3 OKRs 3/3

## Performance

* [Ship large database seeder for developers](https://gitlab.com/gitlab-org/gitlab-ce/issues/28149).
  * Reproduce production-like data locally.
* [Enable Bullet by default on the CI](https://gitlab.com/gitlab-org/gitlab-ce/issues/30129).
  * Detect and avoid N+1 query problems.

---

# Questions?

* What's the Edge team?
  * https://about.gitlab.com/handbook/edge

* Where can I find statistics about our test suite?
  * https://redash.gitlab.com/dashboard/test-suite-statistics

* [Go back to the Edge group conversations](../)

[Robert]: https://gitlab.com/rspeicher
[Mark]: https://gitlab.com/markglenfletcher
[Jen-Shin]: https://gitlab.com/godfat
[Christian]: https://gitlab.com/chriscool

[Grzegorz]: https://gitlab.com/grzesiek
[Balasankar]: https://gitlab.com/balasankarc
[blackst0ne]: https://gitlab.com/blackst0ne
[Alexander Randa]: https://gitlab.com/randaalex

[Build team]: https://about.gitlab.com/handbook/build/
