# Edge update

* Date: 05 December 2017
* Who:
  * [Rémy Coutable](https://gitlab.com/rymai): Edge team lead
* Agenda:
  1. Community
  1. Performance
  1. Triage automation
  1. GitLab QA
  1. Testing
  1. CE->EE merge
  1. Questions?

---

# Community

* Welcome [Takuya Noguchi] to the [core team](https://about.gitlab.com/core-team/)! 🎉

<a href="https://gitlab.com/tnir"><img src="https://gitlab.com/uploads/-/system/user/avatar/100770/canstock6553672_288.png" style="width:100px" /></a>

* We merged [55 MRs from the community in 10.2](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?label_name%5B%5D=Community+Contribution&milestone_title=10.2&sort=created_desc&state=merged)
  * [Check out the spreadsheet](https://docs.google.com/spreadsheets/d/1sdppNaGYh1TkhOFdDkSz20YVhc9z9YKtusW-0pmBOtQ/edit?usp=sharing)

* New [RuboCop rule for line breaks after guard clauses](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/15188), thanks to [Jacopo Beschi]!

* New [destruction services were created for issues and merge requests](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/15604), thanks to [George Andrinopoulos]!

---

# Performance

* The [branches page performance are visible](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14729/diffs) (25% faster, from 4.7s to 3.55s):

  <img src="branches-pages-perf-improvements.png" style="width:400px" />

* A [second performance improvement](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/15628) has been made to this page, thanks to [Jen-Shin]!
  * Improvement will be visible in the first 10.3 RC.

---

# Triage automation

* We now run our [automated triage](https://gitlab.com/gitlab-org/triage) daily for several projects:
  * [GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce/-/jobs/42687147)
  * [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner/-/jobs/42689394)
  * [GitLab.com Support Tracker](https://gitlab.com/gitlab-com/support-forum/-/jobs/42687012)
* [Alessio] from the CI/CD team contributed [a new feature to filter on labels not set](https://gitlab.com/gitlab-org/gitlab-triage/merge_requests/28)!

* The gem can be used in any GitLab project! 🎉
  * That was one of the team's Q4 key result.
  * We need to document that and communicate about this.

---

# GitLab QA

* We've started a documentation: [GitLab QA Architecture](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/docs/architecture.md)
* The build team presented a training:
  * [Video](https://drive.google.com/file/d/1bVt3NPodrzSKlXaNQqUfeyTJDA15G28F/view) and [slides](https://docs.google.com/a/gitlab.com/presentation/d/1-3YlYTIBzd2kSjGVYGPq1xqz4O7qtfABhoZRi_PcGRg/edit?usp=sharing), thanks [Ian]!
* A [lot of activity](https://gitlab.com/gitlab-org/gitlab-qa/activity) and a new [edition upgrade scenario](https://gitlab.com/gitlab-org/gitlab-qa/merge_requests/98).

* The `package-qa` job [now waits for the pipeline trigerred in GitLab Omnibus to finish](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/15538), thanks [Grzegorz]!
  * This is turn can make a MR go red if the QA doesn't pass!
  * Keep in mind this job is still manual.
  * We have a [plan to make GitLab QA production ready](https://gitlab.com/gitlab-org/gitlab-qa/issues/126)!

---

# Testing

* [11 test-related merge requests](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Edge&milestone_title=10.2&label_name[]=test)
  * We have way less transient failures for a few months! 🎉

* Running all tests parallelized takes around 40 minutes on average
  <img src="stable-pipelines.png" style="width:700px" />
  * But we can do even better: [use preemtible instances and parallelize even more](https://gitlab.com/gitlab-com/infrastructure/issues/3131)

---

# CE->EE merge 1/2

* We now have [a script to automatize the CE->EE merge](https://gitlab.com/gitlab-org/release-tools/merge_requests/223) ([doc](https://gitlab.com/gitlab-org/release-tools/blob/master/doc/rake-tasks.md#upstream_merge))

* We now [run this script every 3 hours as a pipeline schedule](https://gitlab.com/gitlab-org/release-tools/merge_requests/223)
  * [This job](https://gitlab.com/gitlab-org/release-tools/-/jobs/43201679) created [this MR](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/3633)

  <img src="automatic-upstream-merge.png" style="width:600px" />

---

# CE->EE merge 2/2

* More improvements will come:
  * [Post link to new CE->EE merge request in the `#ce-to-ee` Slack channel](https://gitlab.com/gitlab-org/release-tools/issues/141)
  * [Automatically assign upstream merge MR to the first person that has to resolve a conflict](https://gitlab.com/gitlab-org/release-tools/issues/148)

---

# Questions?

* What's the Edge team?
  * https://about.gitlab.com/handbook/edge

* Where can I find statistics about our test suite?
  * https://redash.gitlab.com/dashboard/test-suite-statistics

* [Go back to the Edge group conversations](../)

[Robert]: https://gitlab.com/rspeicher
[Mark]: https://gitlab.com/markglenfletcher
[Jen-Shin]: https://gitlab.com/godfat
[Christian]: https://gitlab.com/chriscool

[Nick]: https://gitlab.com/nick.thomas
[Grzegorz]: https://gitlab.com/grzesiek
[Balasankar]: https://gitlab.com/balasankarc
[Yorick]: https://gitlab.com/yorickpeterse
[blackst0ne]: https://gitlab.com/blackst0ne
[Alexander Randa]: https://gitlab.com/randaalex
[Jacopo Beschi]: https://gitlab.com/jacopo-beschi
[Maxim Rydkin]: https://gitlab.com/innerwhisper
[Hiroyuki Sato]: https://gitlab.com/hiroponz
[Build team]: https://about.gitlab.com/handbook/build/
[Axil]: https://gitlab.com/axil
[Markus Koller]: https://gitlab.com/toupeira
[haseeb]: https://gitlab.com/haseebeqx
[Travis Miller]: https://gitlab.com/travismiller
[Richard]: https://gitlab.com/richardc
[Zeger-Jan]: https://gitlab.com/zj
[BJ]: https://gitlab.com/bjgopinath
[Takuya Noguchi]: https://gitlab.com/tnir
[George Andrinopoulos]: https://gitlab.com/geoandri
[Alessio]: https://gitlab.com/nolith
[Ian]: https://gitlab.com/ibaum
