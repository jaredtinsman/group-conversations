# Edge update

* Date: 31 October 2017
* Who:
  * [Rémy Coutable](https://gitlab.com/rymai): Edge team lead
* Agenda:
  1. News
  1. Community
  1. Testing
  1. Performance
  1. QA
  1. Triage automation
  1. Q4 OKRs
  1. Questions?

---

# News

* The Edge team is now part of the Quality department!

* The team is already about working on improving the quality of:
  * Our development workflows
  * Our codebase and technical debt
  * Our issue tracker and community contributions

* The team will spend more time working on GitLab QA (see Q4 OKRs)

* [BJ] will help us prioritize things toward making GitLab.com mission-critical,
  from a Quality point of view

---

# Community 1/4

* We merged [76 MRs from the community in 10.1](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?label_name%5B%5D=Community+Contribution&milestone_title=10.1&sort=created_desc&state=merged)

<iframe width="700" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vRqVY7WOWPEqQ-r0Clik5-Dg8iho2REJJkUiF6yDObwm4zx-DjgT2YzyLmT0A-EPU4oCzA1bFndwbiT/pubchart?oid=16253277&amp;format=interactive"></iframe>

– [Check out the spreadsheet](https://docs.google.com/spreadsheets/d/1sdppNaGYh1TkhOFdDkSz20YVhc9z9YKtusW-0pmBOtQ/edit?usp=sharing)

---

# Community 2/4

* It's now possible to define [Custom attributes](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/13038) for users, thanks to [Markus Koller]!

* Expect [custom attributes on groups and projects soon](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14593).

– [Take a look at the documentation](https://docs.gitlab.com/ee/api/custom_attributes.html)

---

# Community 3/4

* New [creation services were created for SSH/GPG/Deploy Keys](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/13331), thanks to [haseeb]!

* [GitLab Pages version is now displayed in the Admin dashboard](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14040), thanks to [Travis Miller]!

* [Gitaly version is now displayed in the Admin dashboard](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14313), thanks to [Jacopo Beschi]!

---

# Community 4/4

* Decrease [Cyclomatic Complexity threshold to 13](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14152), [Perceived Complexity threshold to 15](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14160), and [ABC threshold to 54.28](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14920)
  * Great effort by [Maxim Rydkin]! Expect [more](https://gitlab.com/gitlab-org/gitlab-ce/issues/31362) [to](https://gitlab.com/gitlab-org/gitlab-ce/issues/31358) [come](https://gitlab.com/gitlab-org/gitlab-ce/issues/28202)!

* [Continue migrating Spinach tests to RSpec](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Edge&label_name[]=backstage&label_name[]=technical%20debt&search=spinach)
  * Thanks [blackst0ne]!

---

# Testing

* We now use High-CPU droplets to run our pipelines.
  * Our test suite is 2x faster and pipelines are way more stable:

  <img src="high-cpu-droplets.png" style="width:500px" />

* [33 test-related merge requests](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Edge&milestone_title=10.1&label_name[]=test)

---

# Performance

* The [branches page performance has been improved greatly](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14729/diffs), thanks to [Jen-Shin]!
  * Improvements will be visible in the first 10.2 RC.
* We now have a ["Performance" category for Changelog items](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14865), thanks to [Zeger-Jan]!

---

# QA

* [QA is now run in a subgroup](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14682/diffs), thanks to [Robert]!
  * A good step toward being able to run QA against staging.
* Great iteration on [testing integration with Mattermost using GitLab QA](https://gitlab.com/gitlab-org/gitlab-qa/issues/26), thanks to [Richard]!
  * [Step 1 in GitLab QA](https://gitlab.com/gitlab-org/gitlab-qa/merge_requests/81/diffs)
  * [Step 2 in GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14818/diffs)
  * [Step 3 in GitLab QA](https://gitlab.com/gitlab-org/gitlab-qa/merge_requests/80/diffs)
  * [Step 4 in GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/15033/diffs)
* [~10 QA Merge Requests](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=QA)
  * GitLab QA is getting more love!

---

# Triage automation

* We now run our [automated triage](https://gitlab.com/gitlab-org/triage) daily.

---

# Q4 OKRs

* Ship [large database seeder for developers](https://gitlab.com/gitlab-org/gitlab-ce/issues/28149)
* Enable [triage](https://gitlab.com/gitlab-org/triage) to be [used for any project](https://gitlab.com/gitlab-org/triage/issues/14#note_41293856)
* Make GitLab QA [test the Container Registry](https://gitlab.com/gitlab-org/gitlab-qa/issues/49)
* Make GitLab QA [test upgrade from CE to EE](https://gitlab.com/gitlab-org/gitlab-qa/issues/64)
* Make GitLab QA [test simple push with PostReceive](https://gitlab.com/gitlab-org/gitlab-qa/issues/53)
* De-duplicate at least 5 redundant (feature) tests
* Improve at least the [5 longest spec files](https://redash.gitlab.com/queries/15) by at least 30%
* Investigate code with less than 60% tests coverage and add tests for at least the [5 most critical files](https://gitlab.com/gitlab-org/gitlab-ce/issues/19412)
* [Investigate encapsulating instance variables](https://gitlab.com/gitlab-org/gitlab-ce/issues/20045) about the current page in a class
* [Reduce duplication](https://gitlab.com/gitlab-org/gitlab-ce/issues/31574) in at least 5 forms
* Solve at least 3 [outstanding performance issues](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&amp;utf8=%E2%9C%93&amp;state=opened&amp;label_name[]=performance&amp;milestone_title=No+Milestone)

---

# Questions?

* What's the Edge team?
  * https://about.gitlab.com/handbook/edge

* Where can I find statistics about our test suite?
  * https://redash.gitlab.com/dashboard/test-suite-statistics

* [Go back to the Edge group conversations](../)

[Robert]: https://gitlab.com/rspeicher
[Mark]: https://gitlab.com/markglenfletcher
[Jen-Shin]: https://gitlab.com/godfat
[Christian]: https://gitlab.com/chriscool

[Nick]: https://gitlab.com/nick.thomas
[Grzegorz]: https://gitlab.com/grzesiek
[Balasankar]: https://gitlab.com/balasankarc
[Yorick]: https://gitlab.com/yorickpeterse
[blackst0ne]: https://gitlab.com/blackst0ne
[Alexander Randa]: https://gitlab.com/randaalex
[Jacopo Beschi]: https://gitlab.com/jacopo-beschi
[Maxim Rydkin]: https://gitlab.com/innerwhisper
[Hiroyuki Sato]: https://gitlab.com/hiroponz
[Build team]: https://about.gitlab.com/handbook/build/
[Axil]: https://gitlab.com/axil
[Markus Koller]: https://gitlab.com/toupeira
[haseeb]: https://gitlab.com/haseebeqx
[Travis Miller]: https://gitlab.com/travismiller
[Richard]: https://gitlab.com/richardc
[Zeger-Jan]: https://gitlab.com/zj
[BJ]: https://gitlab.com/bjgopinath
